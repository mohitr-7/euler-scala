package euler

import scala.Console._

object EulerPrinter {

  def reportSuccess(problem: EulerProblem, message: String): Unit = Console.println(
    s"""
$RESET$BLUE$UNDERLINED${problem.problemHeading}
$BLUE${problem.statement}$BLUE$UNDERLINED
Result$BLUE: $message
$RESET""".stripMargin)

  def reportWarning(problem: EulerProblem, message: String): Unit = Console.println(
    s"""
$RESET$YELLOW$UNDERLINED${problem.problemHeading}
$YELLOW${problem.statement}$YELLOW$UNDERLINED
Result$YELLOW: $message
$RESET""".stripMargin)

  def reportFailure(problem: EulerProblem, message: String): Unit = Console.println(
    s"""
$RESET$RED$UNDERLINED${problem.problemHeading}
$RED${problem.statement}$RED$UNDERLINED
Result$RED: $message
$RESET""".stripMargin)

  def reportResults(successes: Int, failures: Int): Unit =
    if (failures > 0) {
      Console.println(
        s"""
           |$RESET$RED$REVERSED$successes passed, $failures failed.$RESET""".stripMargin)
    } else {
      Console.println(
        s"""
           |$RESET$BLUE$REVERSED$successes passed, 0 failed.$RESET""".stripMargin)
    }

  def printBreak(): Unit = Console.println("=" * 100)

}
