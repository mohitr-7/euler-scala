package euler

trait EulerProblem {
  val statement: String
  val expectedSolution: Option[String]
  val actualSolution: String

  lazy val problemHeading: String = s"Problem ${this.getClass.getSimpleName.dropRight(1).takeRight(4)}"

  def main(args: Array[String]): Unit = report()

  def report(): Boolean = {
    expectedSolution match {
      case Some(solution) =>
        if (actualSolution == solution) {
          EulerPrinter.reportSuccess(this, solution)
        } else {
          EulerPrinter.reportFailure(this,
            s"Solution incorrect. Expected $solution, got $actualSolution.")
        }
      case None =>
        EulerPrinter.reportWarning(this,
          s"Expected solution undefined, but got '$actualSolution'")
    }

    EulerPrinter.printBreak()

    expectedSolution.getOrElse(actualSolution).equals(actualSolution)
  }
}

