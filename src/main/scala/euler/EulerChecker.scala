package euler

import problems._

object EulerChecker extends App {
  val results = List(
    Problem0001, Problem0002, Problem0003, Problem0004, Problem0005,
    Problem0006, Problem0007, Problem0008, Problem0009, Problem0010,
    Problem0011
  ).map(_.report())

  EulerPrinter.reportResults(results.count(result => result), results.count(result => !result))
}
