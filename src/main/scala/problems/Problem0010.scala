package problems

import euler.EulerProblem
import util.Prime

object Problem0010 extends EulerProblem {
  override val statement: String =
    """
      |The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
      |
      |Find the sum of all the primes below two million.
    """.stripMargin

  override val expectedSolution: Option[String] = Some("142913828922")

  override val actualSolution: String = {
    val sum: BigInt = Prime.primes.filter(_ < 2000000).foldLeft(BigInt(0))(_ + _)

    sum.toString
  }
}
