package problems

import euler.EulerProblem

object Problem0002 extends EulerProblem {
  override lazy val statement: String =
    """
      |Each new term in the Fibonacci sequence is generated by adding the previous
      |two terms. By starting with 1 and 2, the first 10 terms will be:
      |
      |    1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
      |
      |By considering the terms in the Fibonacci sequence whose values do not exceed
      |four million, find the sum of the even-valued terms.
    """.stripMargin

  override lazy val expectedSolution: Option[String] = Some("4613732")

  override lazy val actualSolution: String = {

    def sumEven(f_n1: Int, f_n2: Int, total: Int): Int = {
      // on first call, f_n1 is odd (1) and f_n2 is even (2)

      lazy val f_n3 = f_n2 + f_n1 // odd
      lazy val f_n4 = f_n3 + f_n2 // odd
      lazy val f_n5 = f_n4 + f_n3 // even

      if (f_n3 > 4000000 || f_n4 > 4000000 || f_n5 > 4000000) total
      else sumEven(f_n4, f_n5, total + f_n5)
    }

    sumEven(1, 2, 2) toString
  }
}
