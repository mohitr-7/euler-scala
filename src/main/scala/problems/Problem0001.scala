package problems

import euler.EulerProblem
import util.Series

object Problem0001 extends EulerProblem with Series {
  override lazy val statement: String =
    """
      |If we list all the natural numbers below 10 that are multiples of 3 and 5,
      |we get 3, 5, 6 and 9. The sum of these multiples is 23.
      |
      |Find the sum of all the multiples of 3 or 5 below 1000.
    """.stripMargin

  override lazy val expectedSolution: Option[String] = Some("233168")

  override lazy val actualSolution: String = {
    val numberOfMultiplesOf3: Int = 999 / 3
    val numberOfMultiplesOf5: Int = 999 / 5
    val numberOfMultiplesOf15: Int = 999 / 15

     sumArithmeticTerms(3, 3, numberOfMultiplesOf3) +
      sumArithmeticTerms(5, 5, numberOfMultiplesOf5) -
      sumArithmeticTerms(15, 15, numberOfMultiplesOf15) toString
  }
}
