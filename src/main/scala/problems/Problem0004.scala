package problems

import euler.EulerProblem

object Problem0004 extends EulerProblem{
  override lazy val statement: String =
    """
      |A palindromic number reads the same both ways.
      |The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
      |
      |Find the largest palindrome made from the product of two 3-digit numbers.
    """.stripMargin

  override lazy val expectedSolution: Option[String] = Some("906609")

  override lazy val actualSolution: String = {
    def isPalindrome(n: Int): Boolean = {
      n.toString == n.toString.reverse
    }

    lazy val products = for {
      i <- 101 to 999
      j <- i+1 to 999
    } yield i * j

    products.filter(isPalindrome).max.toString
  }
}
