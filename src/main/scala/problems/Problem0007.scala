package problems

import euler.EulerProblem
import util.Prime

object Problem0007 extends EulerProblem{
  override lazy val statement: String =
    """
      |By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
      |
      |What is the 10 001st prime number?
    """.stripMargin

  override lazy val expectedSolution: Option[String] = Some("104743")

  override lazy val actualSolution: String = Prime(10000).toString
}
