package problems

import euler.EulerProblem

object Problem0006 extends EulerProblem {
  override lazy val statement: String =
    """
      |The sum of the squares of the first ten natural numbers is,
      |
      |  1^2 + 2^2 + ... + 10^2 = 385
      |
      |The square of the sum of the first ten natural numbers is,
      |
      |  (1 + 2 + ... + 10)^2 = 55^2 = 3025
      |
      |Hence the difference between the sum of the squares of the first ten natural numbers
      |and the square of the sum is 3025 − 385 = 2640.
      |
      |Find the difference between the sum of the squares of the first one hundred
      |natural numbers and the square of the sum.
    """.stripMargin

  override lazy val expectedSolution: Option[String] = Some("25164150")

  override lazy val actualSolution: String = {
    val terms = for {
      i <- 1 to 100
      j <- (i + 1) to 100
    } yield i * j * 2

    terms.sum.toString
  }
}
