package problems

import euler.EulerProblem

object Problem0009 extends EulerProblem {
  override val statement: String =
    """
      |A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
      |
      |  a^2 + b^2 = c^2
      |
      |For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.
      |
      |There exists exactly one Pythagorean triplet for which a + b + c = 1000.
      |
      |Find the product abc.
    """.stripMargin

  override val expectedSolution: Option[String] = Some("31875000")

  override val actualSolution: String = {
    val perimeter = 1000

    val triangles = for {
      a <- 1 to perimeter / 2
      b <- 1 to a
      c = perimeter - (a + b)
      if a * a + b * b == (c * c)
    } yield a * b * c

    triangles.head.toString
  }
}
