package problems

import euler.EulerProblem
import util.Prime

object Problem0003 extends EulerProblem {
  override lazy val statement: String =
    """
      |The prime factors of 13195 are 5, 7, 13 and 29.
      |
      |What is the largest prime factor of the number 600851475143?
    """.stripMargin

  override lazy val expectedSolution: Option[String] = Some("6857")

  override lazy val actualSolution: String = Prime.factorise(600851475143.0).max.toString
}
