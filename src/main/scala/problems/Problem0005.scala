package problems

import euler.EulerProblem
import util.Prime

object Problem0005 extends EulerProblem {
  override lazy val statement: String =
    """
      |2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
      |
      |What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
    """.stripMargin

  override lazy val expectedSolution: Option[String] = Some("232792560")

  override lazy val actualSolution: String = {
    val UPPER_LIMIT = 20

    def multiplyLargestPower(primeIndex: Int, product: Int): Int = {
      val prime = Prime(primeIndex)
      if (prime > UPPER_LIMIT)
        product
      else {
        val largestPower = Math.floor(Math.log(UPPER_LIMIT) / Math.log(prime)).toInt
        multiplyLargestPower(primeIndex + 1, product * Math.pow(prime, largestPower).toInt)
      }
    }

    multiplyLargestPower(0, 1).toString
  }
}
