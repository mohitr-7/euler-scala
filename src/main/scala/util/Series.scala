package util

trait Series {
  def sumArithmeticTerms(a: Int, d: Int, n: Int): Int = {
    /*
      * Given a series of N terms, starting with A, each D apart,
      *   sum = A + (A + D) + ... + (A + (N - 1)D)
      *       = N(2A + (N -1)D)/2
      *
      * */
    n * ((2 * a) + ((n - 1) * d)) / 2
  }
}
