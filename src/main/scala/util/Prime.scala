package util

import scala.collection.mutable

object Prime {
  private val MAXIMUM_PRIME = 2100000

  val primes: List[Int] = {
    val primeIndices = mutable.ArrayBuffer.fill((MAXIMUM_PRIME + 1) / 2)(1)

    val intSqrt = Math.sqrt(MAXIMUM_PRIME).toInt
    for (i <- 3 to MAXIMUM_PRIME by 2 if i <= intSqrt) {
      for (nonPrime <- i * i to MAXIMUM_PRIME by 2 * i) {
        primeIndices.update(nonPrime / 2, 0)
      }
    }

    2 :: (for (i <- primeIndices.indices if primeIndices(i) == 1) yield 2 * i + 1).tail.toList.sorted
  }

  def apply(index: Int): Int = primes(index)

  def factorise(n: Double): List[Int] = {
    def factorise(n: Double, factors: List[Int], primeIndex: Int): List[Int] = {
      if (n == 1)
        factors
      else {
        val prime = Prime(primeIndex + 1)

        if (n % prime == 0)
          factorise(n / prime, factors :+ prime, primeIndex + 1)
        else
          factorise(n, factors, primeIndex + 1)
      }
    }

    factorise(n, factors = Nil, primeIndex = 0)
  }

  def factorise(n: Int): List[Int] = factorise(n.toDouble)
}