package util

import org.scalatest.FlatSpec

class SeriesSpec extends FlatSpec with Series {
  "An additional series" should "be summed correctly" in {
    assert(sumArithmeticTerms(1, 1, 5) === 15)
  }
}
